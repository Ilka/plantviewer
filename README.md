# Interactive_Systems: PlantViewer
This project is about visualization of different kinds of plants etc. in the real world. It is an augmented reality app, which is meant to help with the decision to buy and/or plant specific plants. For this, the plant(s) are displayed as augmented objects in the real world. It is making use of Vuforias ground plane. On this plane the user can arbitrarily place the plant(s) and see whether it fits the users imagination or not.


# Testing
If you would like to test my project, you can install it on your phone. You will find it under /app/*.apk . Note, that it is an android app.
If you would only like to see results without installing the app on your phone, please have a look at /showroom, I will put all pictures and videos there.


# Contributing
This is a university project, therefore only the listed author can contribute. Sorry!

# Author
Ilka Kopping

# Acknowledgments
I am thankful for **Miguel Flores Rea**s tips and suggestions.
A special thanks to my familie and friends, getting your feedback for my idea was really motivating. 
