﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction1 : MonoBehaviour
{
    private Touch touch;
    private float movespeed,rotatespeed;
    private Quaternion rotY;
    private float touchTime;
    private bool isFixed;
    private float holdingTime;
    private float fixTime;
    private bool actionDone;

    void Start()
    {
        //Initialize the variables
        rotatespeed = 0.5f;
        movespeed = 0.001f;
        fixTime = 2.0f;
        isFixed = false;
        holdingTime = 0.0f;
        actionDone = false;
    }

    void Update()
    {
        if (Input.touchCount > 0){ //check for touches and handle them
            foreach (Touch touch in Input.touches){
                //check if the touch meets an object
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit)){
                    //handle one-finger gestures: move and fix
                    if (Input.touchCount == 1) {
                        //handle move
                        if ((touch.phase == TouchPhase.Moved) && !isFixed){
                            holdingTime = 0.0f; //Moving menas we do not want to fix the object, so set back to 0
                            transform.position = new Vector3( //transform the objects location
                                transform.position.x + touch.deltaPosition.x * movespeed,
                                transform.position.y,
                                transform.position.z + touch.deltaPosition.y * movespeed
                            );
                        }
                        //handle fix
                        else if (touch.phase == TouchPhase.Stationary){
                            holdingTime += 1.0f; //Not moving means we want to fix the object, so count
                            if (holdingTime == fixTime && !actionDone){ //if holding long enough, fix/unfix the object, but only once!
                                actionDone = true;
                                isFixed = !isFixed;
                            }
                        }
                        //set temporary needed values back
                        else if (touch.phase == TouchPhase.Ended){
                            holdingTime = 0.0f;
                            actionDone = false;
                        }
                    }
                    //handle rotation 
                    else if (Input.touchCount == 2){ 
                        if ( (touch.phase == TouchPhase.Moved)&& !isFixed){
                            transform.Rotate(new Vector3(0f, - touch.deltaPosition.x * rotatespeed, 0f)); //transform the objects orientation
                        }   
                    }
                }
            }
        }
    }
}
