﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class switchIndoorsOutdoors : MonoBehaviour
{
    public void goIndoors(){
        SceneManager.LoadScene("InitialScene");
    }

    public void goOutdoors(){
        SceneManager.LoadScene("Outside");
    }
}
